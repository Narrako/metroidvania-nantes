﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject camera;
    public GameObject player;

    private Vector3 offset; //offset une distance entre le player et la camera
    
    void Start () 
    {
        offset = camera.transform.position - player.transform.position; //calcul l'offset distance entre le player et la camera
    }
    
    void LateUpdate () //appeler apres uptadte chaque frame
    {
        camera.transform.position = player.transform.position + offset; //met la camera au même endroit que le player avec en plus la distance qui les sépares
    }
}
