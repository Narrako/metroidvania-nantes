﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonstreMouv : MonoBehaviour
{
    public int healthMonster , healthMonsterMax , speed , jumpForce;
    public bool MoveRight;
    private Bounds _bounds;
    private BoxCollider2D collider;
    public LayerMask layerPlayer, layerPlatform;
    private float currentHitDistance , distanceCast;
    private Vector2 originMonster , directionMonster; 
    public GameObject targ;

    void Start()
    {
        collider = GetComponent<BoxCollider2D>();
    }

    void Update()
    {
        _bounds = collider.bounds;
        originMonster = transform.position;   
        directionMonster = transform.forward; 
        
        RaycastHit2D hits = Physics2D.CircleCast(originMonster, 6, directionMonster, distanceCast, layerPlayer);
        RaycastHit2D wallHit = Physics2D.Raycast(originMonster ,-Vector2.right , 2f, layerPlatform );

        if (wallHit)
        {    
            Debug.Log(wallHit.collider.name);
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce));
        }
        
        if (healthMonster < healthMonsterMax && hits)
        {
            transform.position = Vector3.MoveTowards(transform.position, targ.transform.position, speed);
        } 
        if (healthMonster == 0)
        {
            Destroy(gameObject);
        }
        else
        {
            if(MoveRight) //si le monstre se déplace vers la droite
            {
                transform.Translate(2 * Time.deltaTime * speed, 0, 0);     //il ira vers la gauche
                transform.localScale = new Vector3(0.37825f, transform.localScale.y, transform.localScale.z);
            }
            else //sinon
            {
                transform.Translate(-2 * Time.deltaTime * speed, 0, 0); // il ira vers la droite
                transform.localScale = new Vector3(0.37825f, transform.localScale.y, transform.localScale.z);
            }
        }
    } 
    
    private void OnDrawGizmosSelected()  //pour voir le raycast en rouge
    {
        Gizmos.color = Color.red;
        Debug.DrawLine(originMonster, originMonster + -Vector2.right* distanceCast);
        Gizmos.DrawWireSphere(originMonster + directionMonster * distanceCast, 6);
    }

    private void OnTriggerEnter2D(Collider2D collision) //pour faire changer de sens le monstre (pour sa round)
    {
        if(collision.gameObject.CompareTag("Turn"))  //quand collision (avec game object tag Turn)
        {
            if (MoveRight)
                MoveRight = false;        //il va soit tourné à gauche au à droite en fonction d'un booleen qui s'alterne
            else
                MoveRight = true;
        }

        if (collision.gameObject.CompareTag("Sword"))
        {
            healthMonster--;
        }
    }
}
