﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Attaque : MonoBehaviour
{
    private BoxCollider2D _boxCollider2D;

    public void Start()
    {
        StartCoroutine("attack"); //intitialise la coroutine
        _boxCollider2D = GetComponent<BoxCollider2D>(); //récupère le componement box collider 2D
    }

    public void Update()
    {
        if (Input.GetKeyUp(KeyCode.A)) //si le player attaque
            StartCoroutine("attack"); //lance la coroutine attaque
    }
    
    IEnumerator attack()
    {
        _boxCollider2D.enabled = true; //active le componement box collider 2D
        yield return new WaitForSecondsRealtime(0.25f); //attends le temps de l'animation
        
        
        
        _boxCollider2D.enabled = false; //désactive le compnement
    }
}

