﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class PlayerMovement : MonoBehaviour
{
    public float maxSpeed = 10f; //Vitesse max du perso
    bool facingRight = true; //sens de peso / de l'image

    Animator anim;

    public bool grounded = false; //savoir quand le perso est au sol ou pas
    public Transform groundCheck;
    public float groundRadius = 0.2f; //le cercle au pied du perso
    public LayerMask whatIsGround; //pour dire au perso "c'est quoi le sol exactement"

    public float jumpForce = 700f;
    bool doubleJump = false; //empecher le double saut par défaut

    public GameObject powerUpJump;
    
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround); //controle si il y a le sol ou pas
        anim.SetBool("Ground", grounded);

        if (powerUpJump.active)
        {
            if (grounded)
                doubleJump = false;
        }
          

        anim.SetFloat("vSpeed", GetComponent<Rigidbody2D>().velocity.y);

        float move = Input.GetAxis("Horizontal"); //sens où il se déplace

        anim.SetFloat("Speed", Mathf.Abs(move)); //abcisse car on s'en fou si c'est négatif ou pas

        GetComponent<Rigidbody2D>().velocity = new Vector2(move * maxSpeed, GetComponent<Rigidbody2D>().velocity.y); //instauré la vitesse et sur quel axe

        if (move > 0) //pour détecter le sens du perso
            transform.localScale = new Vector3(0.5288594f, transform.localScale.y, transform.localScale.z);

        else if (move < 0)
            transform.localScale = new Vector3(-0.5288594f, transform.localScale.y, transform.localScale.z);
    }

    private void Update() //appeler à chaque frame
    {
        if ((grounded || !doubleJump) && Input.GetKeyDown(KeyCode.Space)) //autorise le double jump si il ne touche pas le sol
        {
            anim.SetBool("Ground", false);
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce));
            
            if (!doubleJump && !grounded)
                doubleJump = true;
        }

        if(Input.GetKeyDown(KeyCode.A))
            anim.SetBool("attack", true);
        else
        {
            anim.SetBool("attack",false);
        }
    }
}
