﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneToLoad : MonoBehaviour
{
    public string sceneToLoad;
    
    //pour chanegr de scène
    public void loadScene()
    {
        Application.LoadLevel(sceneToLoad);
    }
}
