﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public float MaxHealth;
    public static float numberPv;
    public Slider healthbar;
    public GameObject gameover;
    
    public Rigidbody2D rbPlayer;
    Vector3 newForce;
    private Vector3 newForceHurt;
        
    Animator anim;

    public void Start()    
    {
        gameover.SetActive(false);
        MaxHealth = 100;
        numberPv = MaxHealth;
        healthbar.value = CalculateHealth();
        
        newForce = new Vector3(0, 14, 0);
        newForceHurt = new Vector3(100000, 0, 0);

        anim = GetComponent<Animator>();
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Pique")
        {
            anim.SetBool("Hurt", true);
            DealDamage(10);
            rbPlayer.AddForce(newForce, ForceMode2D.Impulse);
            
        }

        //collision monstre
        
        else
        {
            anim.SetBool("Hurt", false);
        }
   
        if (numberPv <= 0)
        {
            Destroy(gameObject);
            gameover.SetActive(true);
            Time.timeScale = 0f;
        }
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Monstre")
        {
            anim.SetBool("Hurt", true);
            DealDamage((20)); 
            rbPlayer.AddForce(new Vector2(10000f, 10f), ForceMode2D.Impulse);
            Debug.Log("Touche");
        }
    }

    float CalculateHealth()
    {
        return numberPv / MaxHealth;
    }
    
    void DealDamage(float damageValue)
    {
        numberPv -= damageValue;
        healthbar.value = CalculateHealth();
    }
}
